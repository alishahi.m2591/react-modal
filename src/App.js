import React, { useState } from "react";
import styles from "./component/user/styles.css";
import Modal from "./component/modal/modal";

function App () {
    let [displayModal, updateDisplayStatus] = useState(false);
    const submitHandler = ({email,password})=>{
        console.log({email,password});
        // form data ready for sending to dataBase
    };
    return (
        <div style={styles}>
            <button onClick={(e)=>{updateDisplayStatus(true)}}>show modal</button>
            <Modal title="Hello Mary" onClose={(e)=>{updateDisplayStatus(false)}} displayModal={displayModal} onSubmit={submitHandler} />
        </div>
    );

}

export default App;
