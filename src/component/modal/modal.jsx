import React from 'react';
import "./modal.css";
import Form from "./form";


export default function Modal(props) {
    return(
        <div className={ `modalOverlay ${props.displayModal ? 'modalShow ': 'modalHide'}`} >
            <div className="modal" tabIndex="-1" >
                <div className="modal-dialog" >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">{props.title}</h5>
                            <button type="button" onClick={(e)=>{props.onClose(e)}} className=" btn btn-secondary" aria-label="Close">
                             X
                            </button>
                        </div>
                        <div className="modal-body">
                            <Form doSubmit={props.onSubmit} onClose={props.onClose}/>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={(e)=>{props.onClose(e)}} >Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}