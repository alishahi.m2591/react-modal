import React from 'react';

export default function Form({doSubmit, onClose}) {

    const getFormData = (e) => {
        e.preventDefault();
        const email = document.getElementById('exampleInputEmail1').value;
        const password = document.getElementById('exampleInputPassword1').value;
        doSubmit({email,password});
        onClose(e);
    };

    return(
        <form method="post" onSubmit={(e)=>{getFormData(e)}}>
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">Email address</label>
                <input type="email" className="form-control" id="exampleInputEmail1"
                       aria-describedby="emailHelp" placeholder="Enter email"/>
                <small id="emailHelp" className="form-text text-muted">We'll never share your email
                    with anyone else.
                </small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleInputPassword1">Password</label>
                <input type="password" className="form-control" id="exampleInputPassword1"
                       placeholder="Password"/>
            </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            <button type="button" className="btn btn-secondary" onClick={(e)=>{onClose(e)}} >Close</button>
        </form>
    )
}